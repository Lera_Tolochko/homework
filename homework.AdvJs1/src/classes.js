//   Теоретичні питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.

//   Якщо у об'єкту A прототипом є об'єкт В, та у обєкту B є якась властивість, 
//   об'єкт A успадкує туж саму влостивість. 
//   І нам не потрібно повторювати всю інформацію про A, яку він успадковує від  B.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

//   super() викликається для того щоб успадкувати та ініціалізувати властивості батьківського класу. 
//   При цьому у виклику super() потрібно передати в якості аргументів ті ж самі аргументи, 
//   що отримує конструктор класу-нащадка. 


// Клас Employee
class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    get name() {
        return this._name;
    }
  
    get age() {
        return this._age;
    }
  
    get salary() {
        return this._salary;
    }
  

    set name(name) {
        this._name = name;
    }
  
    set age(age) {
        this._age = age;
    }
  
    set salary(salary) {
        this._salary = salary;
    }
}
  

  // Клас Programmer, який успадковує Employee
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.lang = lang;
    }
  
    get salary() {
      return this._salary * 3;
    }
}

  // Створюємо екземпляри класу Programmer
  const programmer1 = new Programmer("Lera", 17, 1000, ["PHP", "Python"]);
  const programmer2 = new Programmer("Misha", 47, 20000, ["C++", "JavaScript"]);
  

  console.log(programmer1);
  console.log(programmer2);

  // це поза завданням - перевірка перезапису salary
  console.log(programmer1.salary);
  console.log(programmer2.salary);

