/* ## Теоретичне питання
    Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

  Відповідь.
  AJAX це модель, яка поєднує в собі технології веб - розробки, такі як HTML, 
  CSS, JavaScript, DOM, та деякі інші. Коли ці технології об'єднуються в модель AJAX, 
  веб-додатки працюють швидше за рахунок відсутності необхідності повного 
  перезавантаження сторінки браузером.



/* ## Завдання 
Отримати список фільмів серії `Зоряні війни` та вивести на екран список персонажів для кожного з них.
*/

const API = "https://ajax.test-danit.com/api/swapi/films";

sendRequest(API)
.then(response => response.json())
.then(data => {
  renderFilms(data);
  return data;
})
.catch(error => console.error(error));


function sendRequest(url) {
  return fetch(url);
}

function renderFilms(data) {
  data.forEach(({ characters, episodeId, name, openingCrawl }) => {
    //- Як тільки отримаємо з сервера інформацію про фільми, 
    // відразу виводимо список усіх фільмів на екран.
    const containerFilm = document.createElement('div');
    containerFilm.innerHTML = `
        <h2>Episode ${episodeId}: ${name}</h2>
        <div class="characters-loader"><span class="loader"></span></div>
        <ul class="characters-list"></ul>
        <p>${openingCrawl}</p>
      `;
    document.body.append(containerFilm);  
    
    showCharacters()

    function showCharacters() {
      const charactersList = containerFilm.querySelector('.characters-list');
      const loaderContainer = containerFilm.querySelector('.characters-loader');

      Promise.all(characters.map(url => fetch(url) //- Для кожного фільму отримуємо з сервера список персонажів, які були показані у цьому фільмі.
        .then(response => response.json())))
        .then(characters => { //- Як тільки отримаємо з сервера інформацію про персонажів будь-якого фільму, 
          // одразу виводимо цю інформацію на екран під назвою фільму.
          charactersList.innerHTML = characters.map(character => `<li>${character.name}</li>`).join('');
          loaderContainer.style.display = 'none';
        })
  
        .catch(error => {
          console.error(error);
          charactersList.innerHTML = '<li>Error loading characters</li>';
          loaderContainer.style.display = 'none';
        });
    }
  }); 
}

