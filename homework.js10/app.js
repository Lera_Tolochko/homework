const tabs = document.querySelector(".tabs");
const description = document.querySelector(".tabs_content");

const getActiveName = () => {
  return tabs.querySelector(".tabs_title_active").dataset.tab;
};

const setActiveDescription = () => {
  if (description.querySelector(".tabs_description_active")) {
    description
      .querySelector(".tabs_description_active")
      .classList.remove("tabs_description_active");
  }
  description
    .querySelector(`[data-tab=${getActiveName()}]`)
    .classList.add("tabs_description_active");
};

if (!tabs.querySelector(".tabs_title_active")) {
  tabs.querySelector(".tabs_title").classList.add("tabs_title_active");
}


setActiveDescription(getActiveName);

tabs.addEventListener("click", (event) => {
  const action = event.target.closest(".tabs_title");
    if (!action) return;
    
    if (action.classList.contains("tabs_title_active")) return;
  
    if (tabs.querySelector(".tabs_title_active")) {
    tabs
      .querySelector(".tabs_title_active")
      .classList.remove("tabs_title_active");
  }

  action.classList.add("tabs_title_active");

    
  setActiveDescription(getActiveName);
})



