// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
  
// setTimeout() запускається одноразово, через зазначений проміжок часу.
// setInterval() запускається неодноразово, а періодично через зазначений інтервал часу.


// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

// Вона не спрацює миттєво, тому що при нульовій затримці виклик функції буде заплановано одразу після виконання поточного коду.


// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

// Тому що зовнішні змінні існують в пам'яті до того часу, поки існує функція, вони можуть займати більше пам'ті, ніж функція. 



const btnStop = document.querySelector("#btn_stop");
const btnStart = document.querySelector("#btn_start");

let slideIndex = 0;
showSlides();

let interval = setInterval(showSlides, 3000);

btnStop.addEventListener("click", () => {
  clearInterval(interval);
  btnStart.removeAttribute("disabled");
  btnStop.setAttribute("disabled", true);
});
btnStart.addEventListener("click", () => {
  setInterval(showSlides, 3000);
  btnStart.setAttribute("disabled", true);
  btnStop.removeAttribute("disabled");
});

function showSlides() {
  let i;
  const slides = document.getElementsByClassName("image_to_show");

  for (i = 0; i < slides.length; i++) {
    slides[i].classList.add("hidden");
  }
  slideIndex++;
  if (slideIndex > slides.length) slideIndex = 1;
  slides[slideIndex - 1].classList.remove("hidden");
}
