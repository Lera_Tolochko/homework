
function filterBy(date, type) {
    const newArray = [];

    for (let anyDate of date) {
        if (typeof anyDate !== type)
        {
            if (anyDate !== null || type !== 'null')
                newArray.push(anyDate);
        }
    }
    return newArray;
}

console.log(filterBy([false, null, 23, '11', 'hi', undefined, {}, []], 'boolean'));
console.log(filterBy([false, null, 23, '11', 'hi', undefined, {}, []], 'null'));
console.log(filterBy([false, null, 23, '11', 'hi', undefined, {}, []], 'number'));
console.log(filterBy([false, null, 23, '11', 'hi', undefined, {}, []], 'string'));
console.log(filterBy([false, null, 23, '11', 'hi', undefined, {}, []], 'undefined'));
console.log(filterBy([false, null, 23, '11', 'hi', undefined, {}, []], 'object'));



