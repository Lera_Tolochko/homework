// ## Теоретичні питання
/* 
1. Опишіть своїми словами що таке Document Object Model (DOM)

    DOM – це деревоподібне відтворення веб-сайту, у вигляді об'єктів, вкладених один в одного. 
    Таке дерево потрібне для правильного відображення сайту та внесення змін на сторінках за допомогою JavaScript

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

    innerHtml повертає весь текст, включаючи HTML-теги, що містяться в елементі.
    innerText повертає весь текст, який міститься в елементі та у всіх його дочірніх елементах як звичайний текст без будь-якого форматування.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
    
    getElementById() - звернення до елементу за унікальнім id;
    getElementsByName() - пошук серед елементів з атрибутом name, повертає список всіх елементів, чий атрибут name задовольняє запиту;
    getElementsByClassName() - звернення до всіх елементів з однаковою назвою классу;
    getElementsByTagName() - звернення до всіх елементів з однаковою назвою тегу;
    querySelector() - шукає та повертає перший елемент, що задовольняє даному CSS-селектору;
    querySelectorAll() - повертає всі елементи, що задовольняють даномму CSS-селектору.
*/


// 1
const p = document.body.querySelectorAll('p');
p.forEach((el) => {
    el.style.backgroundColor = '#ff0000'
});

// 2
const id = document.getElementById('optionsList');
console.log(id);

const parent = id.parentNode;
console.log(parent);

if (id.hasChildNodes()) {
  let child = id.childNodes;
  for (let i = 0; i < child.length; ++i) {
    console.log(
      `${i+1}: name - ${id.childNodes[i].nodeName}, type - ${id.childNodes[i].nodeType}`
    );
  }
}

// 3
const changeParagraph = document.getElementsByClassName('testParagraph');
console.log(changeParagraph);
// Немає такого классу, але є такий id. Тому зробимо з ним

const changeId = document.getElementById('testParagraph');
changeId.innerText = 'This is a paragraph';
console.log(changeId);

// 4,5
let addNewClass = document.querySelector('.main-header').querySelectorAll('li');
console.log(addNewClass); 
for (let i = 0; i < addNewClass.length; i++) {
    addNewClass[i].className = 'nav-item';
}

// 6
const deleteElement = document.querySelectorAll('.section-title');
for (let elem of deleteElement) {
    elem.classList.remove('section-title')
}
