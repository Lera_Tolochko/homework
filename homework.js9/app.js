// Теоретичні питання

// Опишіть, як можна створити новий HTML тег на сторінці.
// document.createElement();

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
    // insertAdjacentHTML(position, text);
    // Перший параметр це кодове слово, яке вказує куди вставляти відносно elem.
    //     "beforebegin" – вставити text безпосередньо перед elem,
    //     "afterbegin" – вставити text в elem, на початку,
    //     "beforeend" – вставити text в elem, в кінці,
    //     "afterend" – вставити text безпосередньо після elem.

    // Як можна видалити елемент зі сторінки?
        // div.remove();  


function createList(array, parent = document.body) {

    let ul = document.querySelector('ul');

    array.forEach(element => {
            let li = document.createElement('li');
            li.append(element);
            ul.append(li);
            
    });
    parent.prepend(ul)
    }
createList(['banana', 'orange', 'apple', 'watermelon', 'kiwi', 'grape', 'melon']);



