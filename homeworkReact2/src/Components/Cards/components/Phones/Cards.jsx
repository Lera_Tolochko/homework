/* import { useState, useEffect } from "react"; */
import { Component } from 'react';
import { sendRequest } from "../../../../Helpers/getData";
import PropTypes from 'prop-types';
import "./Cards.scss"

class Cards extends Component {
   state = { phones: [], }

   componentDidMount() {
      sendRequest('./data.json')
         .then((result) => {
            this.setState(prevState => ({
               ...prevState,
               phones: result
            }))
         })
   }


   render() {
      const { phones } = this.state
      const { handlerFavorite, handlerBasket, openModal, favorites } = this.props
      const favoriteId = favorites.map(({ id }) => {
         return id
      })

      return (
         <>
            {phones.map(({ name, img, article, color, id, price }) => (
               <div key={id} id={id} className='item'>
                  <img src={img} alt="photo" />
                  <p className='item--header'>{name}
                     <span className='favorite-icon'
                     >
                        <svg height="23" width="23" fill='grey' className={favoriteId.includes(id) ? "star2" : "star"} onClick={() => {
                           handlerFavorite({ name, img, article, color, id, price });
                        }} >
                           <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
                        </svg>
                     </span>
                  </p>
                  <p className='item--color'> Color: {color}</p>
                  <p className='item--article'>Article: {article}</p>
                  <p className='item--price'>Prise: {price} ₴</p>
                  <button className='item--button' onClick={() => {
                     openModal()
                     handlerBasket({ name, img, article, color, id, price });
                  }}>Add to basket</button>
               </div>
            ))}
         </>
      )
   }
}

Cards.propTypes = {
   handlerFavorite: PropTypes.func,
   handlerBasket: PropTypes.func,
   openModal: PropTypes.func,
}

export default Cards