import { Component } from 'react';
import PropTypes from 'prop-types';
import './Header.scss'

class Header extends Component {
   render() {
      const { countFavorite, countBasket } = this.props

      return (
         <header className='header'>
            <div className='header__nav'>
               <div className="header__nav--item">
                  <img className='header__logo' src='/img/logo/Mobile-shop-logo-design-on-transparent-background-PNG.png' alt='logo' />
                  <div className='header__nav--item--icon'>
                     <div className='header__nav--item--icon--basket'>
                        <span className='count-basket'>{countBasket}</span>
                        <svg width="36" height="36" fill="currentColor" class="bi bi-basket" viewBox="0 0 16 16"> <path d="M5.757 1.071a.5.5 0 0 1 .172.686L3.383 6h9.234L10.07 1.757a.5.5 0 1 1 .858-.514L13.783 6H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1v4.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 13.5V9a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h1.217L5.07 1.243a.5.5 0 0 1 .686-.172zM2 9v4.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V9H2zM1 7v1h14V7H1zm3 3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 4 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 6 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3A.5.5 0 0 1 8 10zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5zm2 0a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 1 .5-.5z" fill="white"></path> </svg>
                     </div>
                     <div className='header__nav--item--icon--favorites'>
                        <span className='count-favorites'>{countFavorite}</span>
                        <svg width="36" height="36" fill="white" viewBox="0 0 16 16"> <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" /> </svg>
                     </div>
                  </div>
               </div>
            </div>
            <h1 className='header__name'>CoolPhone</h1>
         </header>
      )
   }
}


Header.propTypes = {
   countBasket: PropTypes.number,
   countFavorite: PropTypes.number
};

export default Header