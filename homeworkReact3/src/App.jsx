import './App.scss';
import { Routes, Route } from 'react-router-dom';
import { Home, Cart, Favs } from "./pages";
import { Header } from './components/header';
import { useState, useEffect } from 'react';
import { useFetch, useGetDataFromLS } from './hooks';

function App() {

	let lsBasket = useGetDataFromLS("cardsBasket");
	let lsFavs = useGetDataFromLS("cardsFave");

	const [isModal, setIsModal] = useState(false);
	const [selectedCard, setSelectedCard] = useState(null);
	const [favsCards, setFavsCards] = useState(lsFavs);
	const [basketCards, setBasketCards] = useState(lsBasket);

	const { data, loading, error } = useFetch('/data.json');

	useEffect(() => {
		localStorage.setItem("cardsBasket", JSON.stringify(basketCards));
	}, [basketCards]);

	useEffect(() => {
		localStorage.setItem("cardsFave", JSON.stringify(favsCards));
	}, [favsCards]);

	const addToBasket = (card) => {
		const index = basketCards.findIndex(item => item.article === card.article);

		if (index === -1) {
			const newCardsBasket = [...basketCards, card];
			setBasketCards(newCardsBasket);
			toggleModal();
		}
	};

	const removeFromBasket = (card) => {
		const newCardsBasket = basketCards.filter(item => item.article !== card.article);
		setBasketCards(newCardsBasket);
	};

	const addToFavorite = (card) => {
		if (card && card.article) {
			const index = favsCards.findIndex(item => item.article === card.article);

			if (index !== -1) {
				const newFavsCards = favsCards.filter(item => item.article !== card.article);
				setFavsCards(newFavsCards);
			} else {
				const newFavsCards = [...favsCards, card];
				setFavsCards(newFavsCards);
			}
		}
	};

	const toggleModal = (article) => {
		if (selectedCard === article) {
			setSelectedCard(null);
			setIsModal(false);
		} else {
			setSelectedCard(article);
			setIsModal(true);
		}
	};

	return (
		<div className='wrapper'>
			<Header basketCards={basketCards} favsCards={favsCards} />
			<main className='page'>
				<section className='page__cards cards'>
					<div className="cards__container container">
						<Routes>
							<Route path='/' element={<Home
								data={data}
								loading={loading}
								error={error}
								toggleModal={toggleModal}
								addToBasket={addToBasket}
								addToFavorite={addToFavorite}
								favsCards={favsCards}
								basketCards={basketCards}
								isModal={isModal}
								selectedCard={selectedCard}
							/>} />
							<Route path='/cart' element={<Cart
								removeFromBasket={removeFromBasket}
								basketCards={basketCards}
								addToFavorite={addToFavorite}
								toggleModal={toggleModal}
								isModal={isModal}
								selectedCard={selectedCard}
								favsCards={favsCards}
							/>} />
							<Route path='/favs' element={<Favs
								toggleModal={toggleModal}
								addToBasket={addToBasket}
								addToFavorite={addToFavorite}
								isModal={isModal}
								favsCards={favsCards}
								basketCards={basketCards}
								selectedCard={selectedCard}
							/>} />
						</Routes>
					</div>
				</section>
			</main>
		</div>
	);
}

export default App
