import { cardsTypes } from "../types";

export function fetchDataStart() {
	return {
		type: cardsTypes.FETCH_DATA_START
	}
};

export function fetchDataSuccess(data) {
	return {
		type: cardsTypes.FETCH_DATA_SUCCESS,
		payload: data
	}
};

export function fetchDataError(error) {
	return {
		type: cardsTypes.FETCH_DATA_ERROR,
		payload: error
	}
};

export function getCardsAsync() {
	return async function (dispatch) {
		dispatch(fetchDataStart());
		await fetch("/data.json")
			.then((res) => res.json())
			.then((data) => {
				dispatch(fetchDataSuccess(data));
			})
			.catch((error) => {
				dispatch(fetchDataError(error))
			})
	}
}