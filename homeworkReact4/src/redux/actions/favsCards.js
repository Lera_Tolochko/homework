import { favsCardsTypes } from "../types";

export function setFavsCards(favsCards) {
	return {
		type: favsCardsTypes.SET_FAVS_CARDS,
		payload: favsCards,
	}
}

export function updateFavsCards(favsCards) {
	return function (dispatch) {
		localStorage.setItem("cardsFave", JSON.stringify(favsCards));
		dispatch(setFavsCards(favsCards))
	};
}