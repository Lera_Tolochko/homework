import { isModalTypes } from "../types";

export function isModal() {
	return {
		type: isModalTypes.IS_MODAL,
	}
}