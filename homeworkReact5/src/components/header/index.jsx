import { NavLink } from "react-router-dom";
import { Basket, HeartNonSolid } from "../icons";
import './style.scss'
import { useSelector } from "react-redux";

export function Header() {

	const { basketCards } = useSelector((state) => state.basketCards);
	const { favsCards } = useSelector((state) => state.favsCards);

	return (
		<header className="header">
			<div className="header__container container">
				<NavLink to="/">
					<div className="header__logo">
						<img src="img/POCO_logo.png" alt="Poco" />
					</div>
				</NavLink>
				<div className="header__nav">
					<NavLink to="/favs">
						<div className="header__fave">
							<HeartNonSolid width={35} height={35} />
							<span className="span-fave"
								style={{
									backgroundColor: +favsCards.length !== 0 ? '#e31837' : null,
									padding: +favsCards.length !== 0 ? '4px 8px' : null
								}}>
								{favsCards.length !== 0 ? favsCards.length : ''}
							</span>
						</div>
					</NavLink>
					<NavLink to="cart">
						<div className="header__basket">
							<Basket width={35} height={35} />
							<span className="span-cart"
								style={{
									backgroundColor: +basketCards.length !== 0 ? '#e31837' : null,
									padding: +basketCards.length !== 0 ? '4px 8px' : null
								}}>
								{basketCards.length !== 0 ? basketCards.length : ''}
							</span>
						</div>
					</NavLink>
				</div>
			</div>
		</header>
	)
}