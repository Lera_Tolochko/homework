import { render, fireEvent, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import renderer from "react-test-renderer";
import { Button } from "../button";

afterEach(cleanup);

test("Button calls onClick when clicked", () => {
	const onClickMock = jest.fn();
	const { getByText } = render(
	  <Button text="Click me" onClick={onClickMock} />
	);
 
	const button = getByText("Click me");
	fireEvent.click(button);
 
	expect(onClickMock).toHaveBeenCalled();
 });
 
 test("Button displays correct text", () => {
	const { getByTestId  } = render(<Button text="Submit" />);

	const button = getByTestId("submit");
 
	expect(button).toBeInTheDocument();
 });
 
 test("renders correctly", () => {
	const tree = renderer.create(<Button text="Submit" />).toJSON();
	expect(tree).toMatchSnapshot();
 });