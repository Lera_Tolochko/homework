import { render, fireEvent, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import renderer from "react-test-renderer";
import { Modal } from "../modal";

afterEach(cleanup);

test("Modal is displayed when rendered", () => {
	const onCloseModal = jest.fn();
	const { getByTestId } = render(
	  <Modal header="Modal Header" onCloseModal={onCloseModal} />
	);
	const modalContent = getByTestId("modal-content");
 
	expect(modalContent).toBeInTheDocument();
 });
 
 test("Modal is hidden when clicked outside", () => {
	const onCloseModal = jest.fn();
	const { getByTestId } = render(
	  <Modal header="Modal Header" onCloseModal={onCloseModal} />
	);
	const modal = getByTestId("modal");
 
	fireEvent.click(modal);
 
	expect(onCloseModal).toHaveBeenCalledTimes(1);
 });

 test("Modal displays correct data", () => {
	const name = "Product Name";
	const article = "123456";
	const color = "Red";
	const price = 100;
	const { getByText } = render(
	  <Modal
		 header="Modal Header"
		 name={name}
		 article={article}
		 color={color}
		 price={price}
	  />
	);
 
	expect(getByText(name)).toBeInTheDocument();
	expect(getByText(`Артикул: ${article}`)).toBeInTheDocument();
	expect(getByText(`Колір: ${color}`)).toBeInTheDocument();
	expect(getByText(`Ціна: ${price} ₴`)).toBeInTheDocument();
 });

 test("renders correctly", () => {
	const tree = renderer
	  .create(
		 <Modal
			header="Modal Header"
			closeButton={true}
			actions={<button>Submit</button>}
			onCloseModal={() => {}}
			name="Product Name"
			article="123456"
			color="Red"
			price="100"
		 />
	  )
	  .toJSON();
	expect(tree).toMatchSnapshot();
 });