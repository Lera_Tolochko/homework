import "./style.scss";

export function Button(props) {
	const { backgroundColor, text, className, onClick, type } = props;		

	return (
		<button className={className} type={type} style={{backgroundColor: backgroundColor}} onClick={onClick} data-testid="submit">
			{text}
		</button>
	)
}