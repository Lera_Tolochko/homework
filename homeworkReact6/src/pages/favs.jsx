import { useContext } from "react";
import { ProductList } from "../components/product-list";
import { ViewModeContext } from "../context/viewModeContext";

export function Favs(props) {
	const { toggleModal, addToBasket, addToFavorite, selectedCard} = props;
	const {viewMode} = useContext(ViewModeContext)

	return (
		<>
			<h1 className="cards__title">Улюблені товари</h1>
			<div className={!viewMode ? "cards__body" : "home-cards__body"}>
				<ProductList
					isFavorite={true}
					toggleModal={toggleModal}
					addToBasket={addToBasket}
					addToFavorite={addToFavorite}
					selectedCard={selectedCard}
				/>
			</div>
		</>
	);
}