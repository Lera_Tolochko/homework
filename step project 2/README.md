#Advd-Step progect 2 - Forkio
---

######При розробці проекту використовувались такі технології:
* Методолгія найменування класів BEM;
* HTML з використанням семантичних тегів;
* Принцип Mobile first;
* CSS препроцесор SASS SCSS --Variables --Mixin
* Adaptive and responsive design;
* NPM та його модулі;
* Проект був зібраний за допомогою Gulp;

######Склад розробників проекту:

* Гречко Віра
* Толочко Лєра

######Список виконаних робіт:

- Толочко Лєра зверстала блок Revolutionary Editor, секцію Here is what you get та секцію Fork Subscription Pricing.

- Гречко Віра зверстала шапку сайту з верхнім меню та People Are Talking About Fork.
