let ul = document.querySelector(".tabs");
ul.addEventListener("click", function (ev) {
  let data = ev.target.dataset.tab;

  document.querySelector(".active-content").classList.remove("active-content");
  document.querySelector(".active-tab").classList.remove("active-tab");

  document.querySelector(`[data-li = ${data}]`).classList.add("active-content");

  ev.target.classList.add("active-tab");
});



const filterCards = document.querySelectorAll(".portfolio-card");
const portfolioBtns = document.querySelector(".portfolio-btns");
const loadMoreBtn = document.querySelector(".load-more");
const loader = document.querySelector(".loader");
let currFilter = 'all';
let cardsNum = 12;

function showLoader() {
  loadMoreBtn.classList.add("hide");
  loader.classList.remove("hide");
}
function hideLoader() {
  loadMoreBtn.classList.remove("hide");
  loader.classList.add("hide");
}

showCards();

loadMoreBtn.addEventListener("click", (event) => {
  event.preventDefault();

  showLoader();
  setTimeout(()=>{
    cardsNum += 12;
    hideLoader();
    showCards(currFilter);
  }, 3000);
  
});

portfolioBtns.addEventListener("click", function (event) {
  let currentBtn = document.querySelector(".portfolio-btn-active");
  currentBtn.classList.remove("portfolio-btn-active");
  event.target.classList.add("portfolio-btn-active");

  let filterBtns = event.target.dataset.filter;
  cardsNum = 12;
  currFilter = filterBtns;
  showCards(filterBtns);
});
  
function showCards( filter = 'all') {
  let j = 0;

  for(let i = 0; i < filterCards.length; i++) {
    if((filterCards[i].classList.contains(filter) || filter == 'all') && j < cardsNum) {
      j++;
      filterCards[i].classList.remove('hide');
    } else {
      filterCards[i].classList.add('hide');
    }   
  }

  if(j < cardsNum) {
    loadMoreBtn.classList.add("hide");
  } 
  else {
    loadMoreBtn.classList.remove("hide");
  }
}



const swiper = new Swiper(".mySwiper", {
  slidesPerView: 1,
  grabCursor: true,
  loop: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  
  mousewheel: true,
  keyboard: true,
});
